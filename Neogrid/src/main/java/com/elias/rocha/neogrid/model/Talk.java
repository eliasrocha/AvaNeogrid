package com.elias.rocha.neogrid.model;

import com.elias.rocha.neogrid.exception.ConferenciaNeogridException;

public class Talk implements Comparable<Talk>{
	private String titulo;
	private int tempo;
	
	public Talk(String titulo, boolean validaTitulo) throws Exception {
		super();
		this.titulo = titulo;
		this.setTempo(titulo, validaTitulo);
	}
	
	public Talk(String titulo) throws Exception {
		super();
		this.titulo = titulo;
		this.setTempo(titulo, true);
	}

	public String getTitulo() {
		return titulo;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getTempo() {
		return tempo;
	}
	
	public void setTempo(String tempo, boolean validaTitulo) throws Exception {
		if (validaTitulo) {
			if (tempo.endsWith("lightning")) {
				this.tempo = 5;			
			} else {
				try {
					this.tempo = Integer.parseInt(tempo.replaceAll("[^?0-9]", ""));
				} catch (Exception e) {
					throw new ConferenciaNeogridException("Formato do título inválido.");
				}
			}
		}else {
			this.tempo = 0;
		}
	}

	@Override
	public String toString() {
		return titulo;
	}

	public int compareTo(Talk talk) {
		if (talk.getTempo() > this.getTempo()) {
			return -1;
		} else {		
			return 0;
		}
	}
}
