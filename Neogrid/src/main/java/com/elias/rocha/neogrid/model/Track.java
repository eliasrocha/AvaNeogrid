package com.elias.rocha.neogrid.model;

import java.util.List;

public class Track{

	private Manha m;
	private Tarde t;
	private int remainingTime;
	private boolean trackConcluida;
	
	public Track(List<Talk> talks) throws Exception {
		m = new Manha();
		t = new Tarde();
		trackConcluida = (m.Inicializa(talks)) && (t.Inicializa(talks));

		remainingTime = m.getTempoRestante() + t.getTempoRestante();
	}
	
	public Manha getM() {
		return m;
	}
	
	public void setM(Manha m) {
		this.m = m;
	}
	
	public Tarde getT() {
		return t;
	}
	
	public void setT(Tarde t) {
		this.t = t;
	}
	
	public int getRemainingTime() {
		return remainingTime;
	}

	public boolean isTrackConcluida() {
		return trackConcluida;
	}

	@Override
	public String toString() {
		return m.toString() + t.toString() + "\n";
	}
}
