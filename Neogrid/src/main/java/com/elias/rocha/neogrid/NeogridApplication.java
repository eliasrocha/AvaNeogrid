package com.elias.rocha.neogrid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.elias.rocha.neogrid.bean.ConferenciaNeogridBean;

@SpringBootApplication
public class NeogridApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(NeogridApplication.class, args);
		ConferenciaNeogridBean bean = ctx.getBean(ConferenciaNeogridBean.class);
		try {
			bean.ProcessarArquivo();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
