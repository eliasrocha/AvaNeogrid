package com.elias.rocha.neogrid.model;

import java.util.Calendar;

public class Tarde extends Turno {

	public Tarde() {
		super();
		Calendar cal= Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY,13);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		this.horaInicio = cal.getTime();

		cal.set(Calendar.HOUR_OF_DAY,17);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND, 0);
		this.horaFinal = cal.getTime();

		this.tolerancia = 60;
	}

	public void finaliza() throws Exception {
		Talk t = new Talk("Networking Event", false);
		this.addTalk(t);
	}
}
