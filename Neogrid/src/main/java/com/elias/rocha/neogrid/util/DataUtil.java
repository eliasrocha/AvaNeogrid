package com.elias.rocha.neogrid.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DataUtil {
	public static final SimpleDateFormat formatoHora = new SimpleDateFormat("hh:mma");

	public static Date adicionaMinutos(Date data, int minutos) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(data);
		calendar.add(Calendar.MINUTE, minutos);

		return calendar.getTime();

	}

}
