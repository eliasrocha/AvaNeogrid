package com.elias.rocha.neogrid.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.elias.rocha.neogrid.util.DataUtil;

public abstract class Turno{
	
	protected Date horaInicio;
	protected Date horaFinal;
	protected int tempoRestante;
	protected int tempoTotal;
	protected List<Talk> talks;
	protected int tolerancia;
	
	public Turno() {
		super();
		talks = new ArrayList<Talk>();
	}
	
	public boolean Inicializa(List<Talk> t) throws Exception{
		long diferenca = this.horaFinal.getTime() - this.horaInicio.getTime();
		this.tempoRestante = (int) (diferenca / (60 * 1000));
		this.tempoTotal = tempoRestante - this.getTolerancia();
		this.talks = new ArrayList<Talk>();
	
		int indice = 0;
		int tentativas = 0;
		boolean gerarRandom = false;
		Random rand = new Random();
		
		while((tempoRestante > 0) && (t.size() > 0)) {
			indice = t.size() - 1;
			while (t.size() > 0) {
				if (gerarRandom) {
					indice = rand.nextInt(t.size() - 1);
				}

				if (addTalk(t.get(indice))) {
					t.remove(indice);
					break;
				}  
				if (indice > 0) {
					indice--;
				} else {
					break;
				}
			}
			
			if ((indice == 0) && (t.size() > 0) && (!gerarRandom)) {
				if (getTempoRestante() <= getTolerancia()) {
					break;
				}  else { 
					gerarRandom = true;
					tentativas++;
					while (this.talks.size() != 0) {
						t.add(this.talks.get(this.talks.size() - 1));
						removeTalk(this.talks.get(this.talks.size() - 1));
						if (tentativas > t.size() * 2) {
							return false;
						}
					}
				}
			} else if (gerarRandom){
				gerarRandom = false;
			} else if(tentativas > t.size() * 2){
				return false;
			}
		}
		
		if (getTempoRestante() <= getTolerancia()) {
			return true;
		} else {
			return false;
		}
	}
	
	public abstract void finaliza() throws Exception;

	public Date getHoraInicio() {
		return horaFinal;
	}
	
	public void setHoraInicio(Date horaInicio) {
		horaFinal = horaInicio;
	}
	
	public Date getHoraFinal() {
		return horaInicio;
	}
	
	public void setHoraFinal(Date horaFinal) {
		horaInicio = horaFinal;
	}
	
	public int getTempoRestante() {
		return tempoRestante;
	}
	
	public List<Talk> getTalks() {
		return talks;
	}

	public boolean addTalk(Talk talk) {
		if (this.tempoRestante >= talk.getTempo()) {
			this.talks.add(talk);
			this.tempoRestante-=talk.getTempo();
			return true;
		} 
		return false;
	}
	
	public void removeTalk(Talk talk) {
		this.talks.remove(talk);
		this.tempoRestante+=talk.getTempo();
	}

	public int getTolerancia() {
		return tolerancia;
	}

	public void setTolerancia(int tolerancia) {
		this.tolerancia = tolerancia;
	}

	public int getTempoTotal() {
		return tempoTotal;
	}

	@Override
	public String toString() {
		String retorno = "";
		Date horaAtual = horaInicio;
		
		for (Talk talk : talks) {
			retorno+=DataUtil.formatoHora.format(horaAtual) + " " + talk.toString() +"\n";
			horaAtual = DataUtil.adicionaMinutos(horaAtual, talk.getTempo());
		}
		return retorno;
	}
}
