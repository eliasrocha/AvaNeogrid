package com.elias.rocha.neogrid.exception;

public class ConferenciaNeogridException extends Exception {
	public ConferenciaNeogridException(String mensagemErro) {
		super(mensagemErro);
	}

	private static final long serialVersionUID = 7911884992896497337L;

}
