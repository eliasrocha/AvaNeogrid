package com.elias.rocha.neogrid.model;

import java.util.Calendar;

public class Manha extends Turno{

	public Manha() {
		super();
		Calendar cal= Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 9);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		this.horaInicio = cal.getTime();

		cal.set(Calendar.HOUR_OF_DAY,12);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND, 0);
		this.horaFinal = cal.getTime();
	
		this.tolerancia = 0;
	}
	
	public void finaliza() throws Exception {
		Talk t = new Talk("Lunch", false);
		this.addTalk(t);
	}

}
