package com.elias.rocha.neogrid.bean;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import org.springframework.stereotype.Component;

import com.elias.rocha.neogrid.exception.ConferenciaNeogridException;
import com.elias.rocha.neogrid.model.Talk;
import com.elias.rocha.neogrid.model.Track;

@Component
public class ConferenciaNeogridBean {

	private List<Talk> talks = new ArrayList<Talk>();
	private List<Track> tracks = new ArrayList<Track>();
	
	public void ProcessarArquivo() throws Exception{
		int trackCount = 0;
		int talkRemaining = 0;
		ClassLoader classLoader = getClass().getClassLoader();
		FileReader file = null; 
		Scanner scanner = null;
		String saida = "";
		try {
			file = new FileReader(classLoader.getResource("input.txt").getFile());
			scanner = new Scanner(file);
		} catch (Exception e) {
			throw new ConferenciaNeogridException("Não foi possível carregar o arquivo input.txt");
		}

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			Talk talk = new Talk(line);
			if (talk != null) {
				talks.add(talk);
			}
		}
		scanner.close();
		
		if (talks.size() == 0) {
			throw new ConferenciaNeogridException("Arquivo vazio.");
		}
		
		Collections.sort(talks);			

		while (talks.size() > 0) {
			talkRemaining = 0;
			Track t = new Track(talks);

			for (Talk talk : talks) {
				talkRemaining += talk.getTempo();
			}

			if (!t.isTrackConcluida()) {
				throw new ConferenciaNeogridException("Não foi possível preencher o horário completamente.");
			}

			tracks.add(t);

			if (talkRemaining <= (t.getM().getTempoTotal())) {
				for (int i = talks.size() - 1; i== 0; i--) {
					Talk talk = talks.get(i);
					for (Track track : tracks) {
						if (track.getT().getTempoRestante() > talk.getTempo()) {
							track.getT().addTalk(talk);
							talks.remove(talk);
							break;
						}
					}
				}
			} 
		}		

		for (Track t : tracks) {
			t.getM().finaliza();
			t.getT().finaliza();
			trackCount++;
			saida += "Track " + trackCount + ":\n";
			saida += t;
		}

		System.out.println(saida);
	}
}
